{
  "done": [
    {
      "Autoroute": [
        [
          {
            "pin": "R1-2",
            "layer": "F.Cu"
          },
          {
            "pin": "J1-2",
            "layer": "F.Cu"
          }
        ],
        {
          "presort_by_pairwise_detours": false,
          "router_options": {
            "wrap_around_bands": true,
            "squeeze_through_under_bends": true,
            "routed_band_width": 100.0
          }
        }
      ]
    },
    {
      "Autoroute": [
        [
          {
            "pin": "J1-1",
            "layer": "F.Cu"
          },
          {
            "pin": "R1-1",
            "layer": "F.Cu"
          }
        ],
        {
          "presort_by_pairwise_detours": false,
          "router_options": {
            "wrap_around_bands": true,
            "squeeze_through_under_bends": true,
            "routed_band_width": 100.0
          }
        }
      ]
    }
  ],
  "undone": []
}
