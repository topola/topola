<!--
SPDX-FileCopyrightText: 2025 Topola contributors

SPDX-License-Identifier: MIT
-->

# planar-incr-embed

WIP implementation of incrementally finding planar graph embeddings modulo homotopy equivalence with fixed vertex positions.
